package com.octant.testtcpudp;

import android.content.Context;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

public class UdpClientRunnable implements Runnable {

    private DatagramSocket  client;
    private String serverIp = "192.168.0.1";
    private int serverPort = 8080;
    private byte[] senddata_a;
    private boolean sendFlag = false;

    public UdpClientRunnable(String serverIp, int serverPort) {
        this.serverIp = serverIp;
        this.serverPort = serverPort;
    }

    public String getServerIp() {
        return serverIp;
    }

    public int getServerPort() {
        return serverPort;
    }

    @Override
    public void run() {

        try {
            //1.创建DatagtamSocket
            client = new DatagramSocket();
            //2.创建数据报，用于接收客服端发送的数据
            byte[] data = new byte[1024];//创建字节数组，指定数据报的大小
            DatagramPacket packet = new DatagramPacket(data, data.length);//
            while (true) {
                if (client == null){
                    break;
                }
                client.receive(packet);//此方法在收到数据之前会一直阻塞
                String ip = packet.getAddress().getHostName();
                int port = packet.getPort();
                if (ip.equals(serverIp)==false || port != serverPort){
                    if (listener != null) {
                        listener.onUdpClientReceiveNewConnection(ip,port);
                    }
                }
                if (listener != null) {
                    listener.onUdpClientReceiveData(data, packet.getLength(), client.getPort(), ip, port);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            close();
        }
        if (listener != null) {
            listener.onUdpClientClose();
        }
    }

    public void sendData(final byte[] data, final String ip, final int port, ExecutorService cachedThreadPool){
        if (cachedThreadPool != null){
            cachedThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        //1.定义要发送的地址、端口号、数据
                        InetAddress address = InetAddress.getByName(ip);
                        //2.创建数据报，包含响应的数据信息
                        DatagramPacket packet = new DatagramPacket(data, data.length, address, port);
                        //3.响应
                        client.send(packet);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //1.定义要发送的地址、端口号、数据
                        InetAddress address = InetAddress.getByName(ip);
                        //2.创建数据报，包含响应的数据信息
                        DatagramPacket packet = new DatagramPacket(data, data.length, address, port);
                        //3.响应
                        client.send(packet);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    public void close(){
        if (client != null){
            client.close();
        }
        client = null;
    }

    private UdpClientListener listener;

    public void setListener(UdpClientListener listener) {
        this.listener = listener;
    }

    public interface UdpClientListener{
        void onUdpClientReceiveNewConnection(String clientIp,int clientPort);
        void onUdpClientReceiveData(byte[] data,int len,int port,String serverIp,int serverPort);
        void onUdpClientClose();
    }


}
