package com.octant.testtcpudp;

import android.os.Environment;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;

public class TcpClientRunnable implements Runnable {

    private String TAG = "TcpClientRunnable";

    private Socket client = null;
    private String serverIp = "192.168.0.1";
    private int serverPort = 8080;
    private byte[] senddata_a;
    private boolean sendFlag = false;

    public TcpClientRunnable(String serverIp, int port) {
        this.serverIp = serverIp;
        this.serverPort = port;
    }

    public String getServerIp() {
        return serverIp;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void sendData(byte[] data){
        senddata_a = data;
        sendFlag = true;
    }

    @Override
    public void run() {
        InputStream in = null;
        OutputStream out = null;
        try{
            client = new Socket();
            client.connect(new InetSocketAddress(InetAddress.getByName(serverIp),serverPort),100);
            in = client.getInputStream();
            out = client.getOutputStream();
        }catch(Exception e){
            e.printStackTrace();
            if (listener != null){
                listener.onTcpClientClose();
            }
            Log.i(TAG,"onTcpClientClose");
            return;
        }

        if (listener != null){
            listener.onTcpClientConnectServer(true);
        }
        Log.i(TAG,"onTcpClientConnectServer true");

        while (client!= null && client.isClosed()==false && client.isConnected()) {
            try {
                if (in.available()>0) {
                    byte[] b = new byte[1024];    //定义字节数组
                    int len = in.read(b);
                    if (listener != null){
                        listener.onTcpClientReceiveData(b,len,serverIp,serverPort);
                    }
                    Log.i(TAG,"onTcpClientReceiveData,"+HexUtil.bytesToHex(Arrays.copyOf(b,len)));
                }

                if (sendFlag) {
                    out.write(senddata_a);
                    sendFlag = false;
                }

            } catch (Exception e) {
                e.printStackTrace();
                break;
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (listener != null){
            listener.onTcpClientConnectServer(false);
        }
        Log.i(TAG,"onTcpClientConnectServer false");

        if (listener != null){
            listener.onTcpClientClose();
        }
        Log.i(TAG,"onTcpClientClose");
    }

    public void close(){
        if (client != null){
            try {
                client.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        client = null;
    }

    private TcpClientListener listener;

    public void setListener(TcpClientListener listener) {
        this.listener = listener;
    }

    public interface TcpClientListener{
        void onTcpClientConnectServer(boolean flag);
        void onTcpClientReceiveData(byte[] data,int len,String serverIp,int serverPort);
        void onTcpClientClose();
    }
}
